﻿using UnityEngine;

namespace Extensions
{
    public static class Vector3Extensions
    {
        /// <summary>
        /// Clamps Vector3's
        /// </summary>
        /// <param name="value">The initial Vector3</param>
        /// <param name="min">The minimum Vector3</param>
        /// <param name="max">The maximum Vector3</param>
        /// <returns>The clamped Vector3</returns>
        /// <remarks>
        /// Might be moved to a global class in the future in order to be re-used from different classes
        /// </remarks>
        public static Vector3 Clamp(this Vector3 value, Vector3 min, Vector3 max)
        {
            value.x = Mathf.Clamp(value.x, min.x, max.x);
            value.y = Mathf.Clamp(value.y, min.y, max.y);
            value.z = Mathf.Clamp(value.z, min.z, max.z);
            return value;
        }

        /// <summary>
        /// Calculates distance between vector3's (fatser than Vector3.distance)
        /// </summary>
        /// <param name="value">The initial Vector3</param>
        /// <param name="min">The other Vector3</param>
        /// <returns>The distance in float</returns>
        public static float Distance2(this Vector3 value, Vector3 other)
        {
            return Mathf.Sqrt((value - other).sqrMagnitude);
        }
    }
}
