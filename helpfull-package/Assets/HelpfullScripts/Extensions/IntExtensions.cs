﻿using UnityEngine;

namespace Extensions
{
    public static class IntExtensions
    {

        public static int Percentage(this int number, float amount)
        {
            return (int)Mathf.Floor(number / 100 * amount);
        }
    }
}
