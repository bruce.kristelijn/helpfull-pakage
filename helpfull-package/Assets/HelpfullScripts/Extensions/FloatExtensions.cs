﻿
namespace Extensions
{
    public static class FloatExtensions
    {
        /// <summary>
        /// Gets the percentage of the float
        /// </summary>
        public static float Percentage(this int number, float amount, bool floor = false)
        {
            return number / 100 * amount;
        }
    }
}
