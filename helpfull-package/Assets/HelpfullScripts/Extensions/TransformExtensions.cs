﻿using UnityEngine;

namespace Extensions
{
    public static class TransformExtensions
    {
        /// <summary>
        /// Turns off all childs of a transform
        /// </summary>
        public static void TurnOffChildren(this Transform transform, bool active)
        {
            foreach (Transform child in transform)
            {
                child.gameObject.SetActive(active);
            }
        }
    }
}